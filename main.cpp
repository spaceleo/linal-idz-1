#include <iostream>
#include "matrices/matrix.h"
#include "matrices/polymatrix.h"

int main() {
    Matrix m = Matrix::Array2D{
            {3, -5, 5},
            {-1, 3, 1},
            {1, 1, 4}
    };
    Matrix a = Matrix::Array2D{
            {-1},
            {1},
            {1}
    };
    std::cout << m * a << "\n";

    Matrix n = Matrix::Array2D{
            {1, 1, -2},
            {2, 1, -1},
            {-1, 0, 0}
    };
    Matrix b = Matrix::Array2D{
            {2, -2},
            {-1, 4},
            {2, 1}
    };

    std::cout << n * b << "\n";

    return 0;
}
