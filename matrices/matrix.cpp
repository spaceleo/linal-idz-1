#include "matrix.h"

Matrix::Matrix(size_t n, size_t m) {
    if (n == 0) {
        throw std::runtime_error("matrix height cannot be 0");
    }
    if (m == 0) {
        throw std::runtime_error("matrix width cannot be 0");
    }
    matrix_.resize(n, Row(m, 0));
}

Matrix::Matrix(const Array2D &matrix) {
    if (matrix.empty()) {
        throw std::runtime_error("matrix height cannot be 0");
    }
    if (matrix[0].empty()) {
        throw std::runtime_error("matrix width cannot be 0");
    }
    for (const auto &row: matrix) {
        if (row.size() != matrix[0].size()) {
            throw std::runtime_error("all matrix rows must have equal width");
        }
    }

    matrix_ = matrix;
}

Matrix::Matrix(size_t n, Rational x) {
    *this = Matrix(n, n);
    for (size_t i = 0; i < n; ++i) {
        matrix_[i][i] = x;
    }
}

Matrix &Matrix::operator=(const Matrix::Array2D &matrix) {
    if (matrix.empty()) {
        throw std::runtime_error("matrix height cannot be 0");
    }
    if (matrix[0].empty()) {
        throw std::runtime_error("matrix width cannot be 0");
    }
    for (const auto &row: matrix) {
        if (row.size() != matrix[0].size()) {
            throw std::runtime_error("all matrix rows must have equal width");
        }
    }

    matrix_ = matrix;

    return *this;
}

Matrix Matrix::operator+(const Matrix &other) {
    if (getHeight() != other.getHeight()) {
        throw std::runtime_error("matrices have different heights (" + std::to_string(getHeight()) + " != " +
                                 std::to_string(other.getHeight()) + ")");
    }
    if (getWidth() != other.getWidth()) {
        throw std::runtime_error("matrices have different widths (" + std::to_string(getWidth()) + " != " +
                                 std::to_string(other.getWidth()) + ")");
    }
    Matrix result = *this;

    for (size_t i = 0; i < getHeight(); ++i) {
        for (size_t j = 0; j < getWidth(); ++j) {
            result[i][j] += other[i][j];
        }
    }

    return result;
}

Matrix Matrix::operator-(const Matrix &other) {
    if (getHeight() != other.getHeight()) {
        throw std::runtime_error("matrices have different heights (" + std::to_string(getHeight()) + " != " +
                                 std::to_string(other.getHeight()) + ")");
    }
    if (getWidth() != other.getWidth()) {
        throw std::runtime_error("matrices have different widths (" + std::to_string(getWidth()) + " != " +
                                 std::to_string(other.getWidth()) + ")");
    }
    Matrix result = *this;

    for (size_t i = 0; i < getHeight(); ++i) {
        for (size_t j = 0; j < getWidth(); ++j) {
            result[i][j] -= other[i][j];
        }
    }

    return result;
}

Matrix Matrix::operator*(const Matrix &other) {
    if (matrix_[0].size() != other.matrix_.size()) {
        throw std::runtime_error(
                "matrices cannot be multiplied: first matrix 1st dim (" + std::to_string(matrix_[0].size()) +
                ") != second matrix 0 dim (" + std::to_string(other.matrix_.size()) + ")");
    }

    Matrix result_matrix(matrix_.size(), other.matrix_[0].size());

    for (size_t i = 0; i < matrix_.size(); ++i) {
        for (size_t j = 0; j < other.matrix_[0].size(); ++j) {
            for (size_t k = 0; k < matrix_[i].size(); ++k) {
                result_matrix[i][j] += matrix_[i][k] * other.matrix_[k][j];
            }
        }
    }

    return result_matrix;
}

Matrix Matrix::operator+(Rational x) {
    if (getWidth() != getHeight()) {
        throw std::runtime_error(
                "matrix width and matrix height must be equal: (" + std::to_string(getWidth()) + " != " +
                std::to_string(getWidth()) + ")");
    }

    Matrix result = *this;
    for (size_t i = 0; i < getHeight(); ++i) {
        result[i][i] += x;
    }

    return result;
}

Matrix Matrix::operator-(Rational x) {
    if (getWidth() != getHeight()) {
        throw std::runtime_error(
                "matrix width and matrix height must be equal: (" + std::to_string(getWidth()) + " != " +
                std::to_string(getWidth()) + ")");
    }

    Matrix result = *this;
    for (size_t i = 0; i < getHeight(); ++i) {
        result[i][i] -= x;
    }

    return result;
}

Matrix &Matrix::operator*=(Rational x) {
    for (size_t i = 0; i < getHeight(); ++i) {
        for (size_t j = 0; j < getWidth(); ++j) {
            matrix_[i][j] *= x;
        }
    }
    return *this;
}

Matrix &Matrix::operator/=(Rational x) {
    for (size_t i = 0; i < getHeight(); ++i) {
        for (size_t j = 0; j < getWidth(); ++j) {
            matrix_[i][j] /= x;
        }
    }
    return *this;
}

const Row &Matrix::operator[](size_t i) const {
    if (i >= matrix_.size()) {
        throw std::runtime_error(std::to_string(i) + " is more than matrix height");
    }
    return matrix_[i];
}

Row &Matrix::operator[](size_t i) {
    if (i >= matrix_.size()) {
        throw std::runtime_error(std::to_string(i) + " is more than matrix height");
    }
    return matrix_[i];
}

size_t Matrix::getHeight() const {
    return matrix_.size();
}

size_t Matrix::getWidth() const {
    return matrix_[0].size();
}

Matrix Matrix::transpose() const {
    Matrix m(getWidth(), getHeight());
    for (size_t i = 0; i < getHeight(); ++i) {
        for (size_t j = 0; j < getWidth(); ++j) {
            m[j][i] = matrix_[i][j];
        }
    }
    return m;
}

Matrix Matrix::concatenate(const Matrix& other) const {
    if (getWidth() != other.getWidth()) {
        throw std::runtime_error("matrices have different widths (" + std::to_string(getWidth()) + " != " +
                                 std::to_string(other.getWidth()) + ")");
    }

    Array2D ans = matrix_;
    for (const Row& row : other.matrix_) {
        ans.push_back(row);
    }

    return ans;
}

Matrix Matrix::gaussianElimination() const {
    Matrix temp = *this;

    size_t cur = 0;
    for (size_t i = 0; i < temp.getWidth() && cur < temp.getHeight(); ++i) {
        if (temp[cur][i] == 0) {
            for (size_t j = cur + 1; j < temp.getHeight(); ++j) {
                if (temp[j][i] != 0) {
                    std::swap(temp[cur], temp[j]);

                    break;
                }
            }
            if (temp[cur][i] == 0) {
                continue;
            }
        }

        temp[cur] /= temp[cur][i];

        for (size_t j = 0; j < temp.getHeight(); ++j) {
            if (cur == j) {
                continue;
            }
            if (temp[j][i] == 0) {
                continue;
            }

            temp[j] -= temp[cur] * temp[j][i];
        }
        ++cur;
    }

    while (!temp.matrix_.empty() && temp.matrix_.back() == 0) {
        temp.matrix_.pop_back();
    }

    return temp;
}

Row Matrix::getColumn(size_t j) {
    if (j > getWidth()) {
        throw std::runtime_error(
                "index must be less than matrix width: " + std::to_string(j) + " >= " + std::to_string(getWidth()));
    }
    Row column;
    for (size_t i = 0; i < getHeight(); ++i) {
        column.push_back(matrix_[i][j]);
    }

    return column;
}

Matrix::Solution Matrix::getSolution() const {
    Matrix temp = *this;

    size_t cur = 0;
    std::vector<size_t> vars;
    for (size_t i = 0; i < temp.getWidth() && cur < temp.getHeight(); ++i) {
        if (temp[cur][i] == 0) {
            for (size_t j = cur + 1; j < temp.getHeight(); ++j) {
                if (temp[j][i] != 0) {
                    std::swap(temp[cur], temp[j]);

                    break;
                }
            }
            if (temp[cur][i] == 0) {
                continue;
            }
        }
        vars.push_back(i);

        temp[cur] /= temp[cur][i];

        for (size_t j = 0; j < temp.getHeight(); ++j) {
            if (cur == j) {
                continue;
            }
            if (temp[j][i] == 0) {
                continue;
            }

            temp[j] -= temp[cur] * temp[j][i];
        }
        ++cur;
    }

    Array2D weights;
    for (size_t i = 0; i < vars.size(); ++i) {
        weights.push_back(temp[i]);

        weights[i][vars[i]] = 0;
        weights[i] *= -1;
    }

    return {vars, std::vector<Rational>(vars.size()), weights, true};
}

Matrix::Solution Matrix::getSolution(std::vector<Rational> results) const {
    if (results.size() != getHeight()) {
        throw std::runtime_error(
                "results size must be equal to matrix height (" + std::to_string(results.size()) + " != " +
                std::to_string(getHeight()) + ")");
    }
    Matrix temp = *this;

    size_t cur = 0;
    std::vector<size_t> vars;
    for (size_t i = 0; i < temp.getWidth() && cur < temp.getHeight(); ++i) {
        if (temp[cur][i] == 0) {
            for (size_t j = cur + 1; j < temp.getHeight(); ++j) {
                if (temp[j][i] != 0) {
                    std::swap(temp[cur], temp[j]);
                    std::swap(results[cur], results[j]);

                    break;
                }
            }
            if (temp[cur][i] == 0) {
                continue;
            }
        }
        vars.push_back(i);

        results[cur] /= temp[cur][i];
        temp[cur] /= temp[cur][i];

        for (size_t j = 0; j < temp.getHeight(); ++j) {
            if (cur == j) {
                continue;
            }
            if (temp[j][i] == 0) {
                continue;
            }

            results[j] -= results[cur] * temp[j][i];
            temp[j] -= temp[cur] * temp[j][i];
        }
        ++cur;
    }

    for (size_t i = vars.size(); i < getHeight(); ++i) {
        if (results[i] != 0) {
            return {.exists = false};
        }
    }

    Array2D weights;
    std::vector<Rational> biases;

    for (size_t i = 0; i < vars.size(); ++i) {
        weights.push_back(temp[i]);

        weights[i][vars[i]] = 0;
        weights[i] *= -1;

        biases.push_back(results[i]);
    }

    return {vars, biases, weights, true};
}

Matrix Matrix::getFundamentalSolutionSystem() const {
    Solution solution = getSolution();
    Matrix temp = Matrix(solution.weights);

    std::vector<Row> ans;

    size_t current_column = 0;
    for (size_t j = 0; j < temp.getWidth(); ++j) {
        if (current_column < solution.vars.size() && solution.vars[current_column] == j) {
            ++current_column;
            continue;
        }

        Row column = temp.getColumn(j);
        Row vec;
        size_t cur = 0;
        for (size_t i = 0; i < temp.getWidth(); ++i) {
            if (cur < column.size() && solution.vars[cur] == i) {
                vec.push_back(column[cur]);
                ++cur;
                continue;
            }
            vec.push_back(0);
        }

        vec[j] = 1;
        ans.push_back(vec);
    }

    return ans;
}

Matrix Matrix::getImage() const {
    // НЕ РАБОТАЕТ TODO
    return transpose().gaussianElimination();
}

bool Matrix::checkSolution(const Row &row) const {
    if (row.size() != getWidth()) {
        throw std::runtime_error("row size must be equal to matrix width: (" + std::to_string(row.size()) + " != " +
                                 std::to_string(getWidth()) + ")");
    }

    for (size_t i = 0; i < getHeight(); ++i) {
        Rational result = 0;
        for (size_t j = 0; j < row.size(); ++j) {
            result += matrix_[i][j] * row[j];
        }
        if (result != 0) {
            return false;
        }
    }

    return true;
}

Rational Matrix::determinant() const {
    if (getWidth() != getHeight()) {
        throw std::runtime_error(
                "matrix width and matrix height must be equal: (" + std::to_string(getWidth()) + " != " +
                std::to_string(getWidth()) + ")");
    }

    Matrix temp = *this;

    Rational det = 1;

    for (size_t i = 0; i < temp.getWidth(); ++i) {
        if (temp[i][i] == 0) {
            for (size_t j = i + 1; j < temp.getHeight(); ++j) {
                if (temp[j][i] != 0) {
                    std::swap(temp[i], temp[j]);
                    det *= -1;

                    break;
                }
            }
            if (temp[i][i] == 0) {
                return 0;
            }
        }

        det *= temp[i][i];
        temp[i] /= temp[i][i];

        for (size_t j = i + 1; j < temp.getHeight(); ++j) {
            if (temp[j][i] == 0) {
                continue;
            }

            temp[j] -= temp[i] * temp[j][i];
        }
    }

    return det;
}

Rational Matrix::getMinor(size_t i, size_t j) {
    Matrix tmp(getHeight() - 1, getWidth() - 1);

    for (size_t x = 0; x < getHeight() - 1; ++x) {
        for (size_t y = 0; y < getWidth() - 1; ++y) {
            size_t cur_x = x, cur_y = y;
            if (x >= i) {
                ++cur_x;
            }
            if (y >= j) {
                ++cur_y;
            }

            tmp[x][y] = matrix_[cur_x][cur_y];
        }
    }

    return tmp.determinant();
}

Matrix Matrix::inverse() {
    if (getWidth() != getHeight()) {
        throw std::runtime_error(
                "matrix width and matrix height must be equal: (" + std::to_string(getWidth()) + " != " +
                std::to_string(getWidth()) + ")");
    }

    Rational d = determinant();
    if (d == 0) {
        throw std::runtime_error(
                "cannot get inverse of matrix with determinant 0"
        );
    }

    Matrix minors(getHeight(), getWidth());
    for (size_t i = 0; i < getHeight(); ++i) {
        for (size_t j = 0; j < getWidth(); ++j) {
            minors[j][i] = getMinor(i, j);
            if ((i + j) & 1) {
                minors[j][i] *= -1;
            }
        }
    }

    minors /= determinant();

    return minors;
}

std::ostream &operator<<(std::ostream &out, const Matrix &matrix) {
    for (size_t i = 0; i < matrix.getHeight(); ++i) {
        for (size_t j = 0; j < matrix.getWidth(); ++j) {
            out << matrix[i][j] << " ";
        }
        out << "\n";
    }
    return out;
}

std::ostream &operator<<(std::ostream &out, const Matrix::Solution &solution) {
    if (!solution.exists) {
        out << "no solutions\n";
        return out;
    }

    if (solution.vars.empty()) {
        out << "variables can take any values\n";
        return out;
    }

    for (size_t i = 0; i < solution.vars.size(); ++i) {
        out << "x" << solution.vars[i] + 1 << " = (" << solution.biases[i] << ") + ";
        for (size_t j = 0; j < solution.weights[i].size(); ++j) {
            if (solution.weights[i][j] != 0) {
                out << "(" << solution.weights[i][j] << ") * x" << j + 1 << " + ";
            }
        }
        out << "0" << std::endl;
    }

    return out;
}
