//
// Created by spaceleo on 25.10.2021.
//

#ifndef MATRIX_H
#define MATRIX_H

#include "../numeric/rational.h"
#include "row.h"

#include <cmath>
#include <stdexcept>
#include <string>
#include <vector>

class Matrix {
public:
    using Array2D = std::vector<Row>;

    Matrix() = default;

    Matrix(size_t n, size_t m);

    Matrix(const Array2D &matrix);

    Matrix(size_t n, Rational x);

    ~Matrix() = default;

    struct Solution {
        std::vector<size_t> vars;
        std::vector<Rational> biases;
        Array2D weights;

        bool exists;

        friend std::ostream &operator<<(std::ostream &out, const Solution &solution);
    };

    Matrix &operator=(const Array2D &matrix);

    Matrix operator+(const Matrix &other);

    Matrix operator-(const Matrix &other);

    Matrix operator*(const Matrix &other);

    Matrix operator+(Rational x);

    Matrix operator-(Rational x);

    Matrix &operator*=(Rational x);

    Matrix &operator/=(Rational x);

    const Row &operator[](size_t i) const;

    Row &operator[](size_t i);

    [[nodiscard]] size_t getHeight() const;

    [[nodiscard]] size_t getWidth() const;

    [[nodiscard]] Matrix transpose() const;

    [[nodiscard]] Matrix concatenate(const Matrix& other) const;

    Row getColumn(size_t j);

    [[nodiscard]] Matrix gaussianElimination() const;

    [[nodiscard]] Solution getSolution() const;

    [[nodiscard]] Solution getSolution(std::vector<Rational> results) const;

    [[nodiscard]] Matrix getFundamentalSolutionSystem() const;

    [[nodiscard]] Matrix getImage() const;

    [[nodiscard]] bool checkSolution(const Row &solution) const;

    [[nodiscard]] Rational determinant() const;

    Rational getMinor(size_t i, size_t j);

    Matrix inverse();

    friend std::ostream &operator<<(std::ostream &out, const Matrix &matrix);

private:
    Array2D matrix_;
};


#endif //MATRIX_H
