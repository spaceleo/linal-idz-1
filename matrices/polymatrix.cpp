//
// Created by spaceleo on 08.12.2021.
//

#include "polymatrix.h"

PolyMatrix::PolyMatrix(size_t n, size_t m) {
    if (n == 0) {
        throw std::runtime_error("matrix height cannot be 0");
    }
    if (m == 0) {
        throw std::runtime_error("matrix width cannot be 0");
    }
    matrix_.resize(n, Row(m, {0}));
}

PolyMatrix::PolyMatrix(const Matrix &matrix) {
    matrix_.resize(matrix.getHeight(), Row(matrix.getWidth(), Rational(0)));
    for (size_t i = 0; i < matrix.getHeight(); ++i) {
        for (size_t j = 0; j < matrix.getWidth(); ++j) {
            matrix_[i][j] = matrix[i][j];
        }
    }
}

PolyMatrix::PolyMatrix(const Array2D &matrix) {
    if (matrix.empty()) {
        throw std::runtime_error("matrix height cannot be 0");
    }
    if (matrix[0].empty()) {
        throw std::runtime_error("matrix width cannot be 0");
    }
    for (const auto &row: matrix) {
        if (row.size() != matrix[0].size()) {
            throw std::runtime_error("all matrix rows must have equal width");
        }
    }

    matrix_ = matrix;
}

PolyMatrix::PolyMatrix(size_t n, const Polynomial &x) {
    *this = PolyMatrix(n, n);
    for (size_t i = 0; i < n; ++i) {
        matrix_[i][i] = x;
    }
}

PolyMatrix PolyMatrix::operator+(const Polynomial &x) {
    if (getWidth() != getHeight()) {
        throw std::runtime_error(
                "matrix width and matrix height must be equal: (" + std::to_string(getWidth()) + " != " +
                std::to_string(getWidth()) + ")");
    }

    PolyMatrix result = *this;
    for (size_t i = 0; i < getHeight(); ++i) {
        result[i][i] = result[i][i] + x;
    }

    return result;
}

PolyMatrix PolyMatrix::operator-(const Polynomial &x) {
    if (getWidth() != getHeight()) {
        throw std::runtime_error(
                "matrix width and matrix height must be equal: (" + std::to_string(getWidth()) + " != " +
                std::to_string(getWidth()) + ")");
    }

    PolyMatrix result = *this;
    for (size_t i = 0; i < getHeight(); ++i) {
        result[i][i] = result[i][i] - x;
    }

    return result;
}

const PolyMatrix::Row &PolyMatrix::operator[](size_t i) const {
    if (i >= matrix_.size()) {
        throw std::runtime_error(std::to_string(i) + " is more than matrix height");
    }
    return matrix_[i];
}

PolyMatrix::Row &PolyMatrix::operator[](size_t i) {
    if (i >= matrix_.size()) {
        throw std::runtime_error(std::to_string(i) + " is more than matrix height");
    }
    return matrix_[i];
}

size_t PolyMatrix::getHeight() const {
    return matrix_.size();
}

size_t PolyMatrix::getWidth() const {
    return matrix_[0].size();
}

Polynomial PolyMatrix::determinant() const {
    std::set<size_t> used;
    return determinant(0, used);
}

Polynomial PolyMatrix::determinant(size_t i, std::set<size_t>& used) const {
    if (i >= getHeight()) return Rational(1);
    Polynomial det = Rational(0);

    size_t cnt = 0;
    for (size_t j = 0; j < getWidth(); ++j) {
        if (used.count(j)) {
            ++cnt;
            continue;
        }

        used.insert(j);
        Polynomial tmp = determinant(i + 1, used);
        used.erase(j);

        if ((j - cnt) & 1) {
            det = det - tmp * matrix_[i][j];
        } else {
            det = det + tmp * matrix_[i][j];
        }
    }

    return det;
}
