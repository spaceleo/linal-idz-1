//
// Created by spaceleo on 08.12.2021.
//

#ifndef POLYMATRIX_H
#define POLYMATRIX_H

#include "../numeric/polynomial.h"
#include "../matrices/matrix.h"

#include <set>
#include <vector>

class PolyMatrix {
public:
    using Row = std::vector<Polynomial>;
    using Array2D = std::vector<Row>;

    PolyMatrix() = default;

    PolyMatrix(size_t n, size_t m);

    PolyMatrix(const Matrix &matrix);

    PolyMatrix(const Array2D &matrix);

    PolyMatrix(size_t n, const Polynomial &x);

    ~PolyMatrix() = default;

    PolyMatrix operator+(const Polynomial &x);

    PolyMatrix operator-(const Polynomial &x);

    const Row &operator[](size_t i) const;

    Row &operator[](size_t i);

    [[nodiscard]] size_t getHeight() const;

    [[nodiscard]] size_t getWidth() const;

    Polynomial determinant() const;

    Polynomial determinant(size_t i, std::set<size_t>& used) const;

private:
    Array2D matrix_;
};


#endif //POLYMATRIX_H
