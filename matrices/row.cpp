//
// Created by spaceleo on 2/28/22.
//

#include "row.h"

Row::Row() : std::vector<Rational>() {}

Row::Row(size_t n, Rational x) : std::vector<Rational>(n, x) {}

Row::Row(std::initializer_list<Rational> l) : std::vector<Rational>(l) {}

Row Row::operator*(Rational x) {
    Row result = *this;
    for (auto &el: result) {
        el *= x;
    }
    return result;
}

Row &Row::operator+=(const Row &other) {
    if (size() != other.size()) {
        throw std::runtime_error(
                "rows have different lengths: " + std::to_string(size()) + " != " + std::to_string(other.size()));
    }

    for (size_t i = 0; i < other.size(); ++i) {
        (*this)[i] += other[i];
    }

    return *this;
}

Row &Row::operator-=(const Row &other) {
    if (size() != other.size()) {
        throw std::runtime_error(
                "rows have different lengths: " + std::to_string(size()) + " != " + std::to_string(other.size()));
    }

    for (size_t i = 0; i < other.size(); ++i) {
        (*this)[i] -= other[i];
    }

    return *this;
}

Row &Row::operator*=(Rational x) {
    for (auto &el: *this) {
        el *= x;
    }
    return *this;
}

Row &Row::operator/=(Rational x) {
    for (auto &el: *this) {
        el /= x;
    }
    return *this;
}

bool Row::operator==(Rational x) {
    for (auto &el: *this) {
        if (el != x) {
            return false;
        }
    }
    return true;
}

bool Row::operator!=(Rational x) {
    return !(*this == x);
}
