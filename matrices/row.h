#ifndef ROW_H
#define ROW_H

#include "../numeric/rational.h"

#include <vector>

class Row : public std::vector<Rational> {
public:
    Row();
    Row(size_t n, Rational x);
    Row(std::initializer_list<Rational> l);

    Row operator*(Rational x);

    Row &operator+=(const Row &other);

    Row &operator-=(const Row &other);

    Row &operator*=(Rational x);

    Row &operator/=(Rational x);

    bool operator==(Rational x);

    bool operator!=(Rational x);
};


#endif //ROW_H
