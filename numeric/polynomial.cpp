//
// Created by spaceleo on 08.12.2021.
//

#include "polynomial.h"

Polynomial::Polynomial() {
    a_ = {0};
}

Polynomial::Polynomial(Rational x) {
    a_ = {x};
}

Polynomial::Polynomial(const Coefs& a) {
    a_ = a;
    reduce();
}

Polynomial::Polynomial(std::initializer_list<Rational> a) {
    a_ = a;
    reduce();
}

Polynomial Polynomial::operator+(const Polynomial &other) const {
    std::vector<Rational> result(std::max(size(), other.size()));
    for (size_t i = 0; i < result.size(); ++i) {
        if (i < size()) result[i] += a_[i];
        if (i < other.size()) result[i] += other[i];
    }
    return {result};
}

Polynomial Polynomial::operator-(const Polynomial &other) const {
    std::vector<Rational> result(std::max(size(), other.size()));
    for (size_t i = 0; i < result.size(); ++i) {
        if (i < size()) result[i] += a_[i];
        if (i < other.size()) result[i] -= other[i];
    }
    return {result};
}

Polynomial Polynomial::operator*(const Polynomial &other) const {
    std::vector<Rational> result(deg() + other.deg() + 1);

    for (size_t i = 0; i < size(); ++i) {
        for (size_t j = 0; j < other.size(); ++j) {
            result[i + j] += a_[i] * other[j];
        }
    }

    return {result};
}

bool Polynomial::operator==(Rational x) const {
    return a_.size() == 1 && a_[0] == x;
}

bool Polynomial::operator!=(Rational x) const {
    return a_.size() != 1 || a_[0] != x;
}

Rational &Polynomial::operator[](size_t i) {
    return a_[i];
}

const Rational &Polynomial::operator[](size_t i) const {
    return a_[i];
}

size_t Polynomial::deg() const {
    return a_.size() - 1;
}

void Polynomial::reduce() {
    while (a_.size() > 1 && a_.back() == 0) {
        a_.pop_back();
    }
}

size_t Polynomial::size() const {
    return a_.size();
}

std::ostream& operator<<(std::ostream& out, const Polynomial& p) {
    out << p[0];
    for (size_t i = 1; i < p.size(); ++i) {
        out << "+(" << p[i] << ")x";
        if (i > 1) {
            out << "^" << i;
        }
    }

    return out;
}
