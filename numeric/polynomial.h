//
// Created by spaceleo on 08.12.2021.
//

#ifndef UNTITLED1_POLYNOMIAL_H
#define UNTITLED1_POLYNOMIAL_H

#include "rational.h"

#include <algorithm>
#include <iostream>
#include <vector>

class Polynomial {
public:
    using Coefs = std::vector<Rational>;

    Polynomial();
    Polynomial(Rational x);
    Polynomial(const Coefs& a);
    Polynomial(std::initializer_list<Rational> a);

    Polynomial operator+(const Polynomial& other) const;
    Polynomial operator-(const Polynomial& other) const;
    Polynomial operator*(const Polynomial& other) const;

    bool operator==(Rational x) const;
    bool operator!=(Rational x) const;

    Rational& operator[](size_t i);
    const Rational& operator[](size_t i) const;

    friend std::ostream& operator<<(std::ostream& out, const Polynomial& p);

private:
     Coefs a_;

    size_t deg() const;
    void reduce();
    size_t size() const;
};


#endif //UNTITLED1_POLYNOMIAL_H
