//
// Created by spaceleo on 25.10.2021.
//

#include "rational.h"

Rational::Rational() : numerator_(0), denominator_(1) {
}

Rational::Rational(int64_t a) : numerator_(a), denominator_(1) {
}

Rational Rational::operator+(const Rational& other) const {
    Rational result;
    result.numerator_ = numerator_ * other.denominator_ + other.numerator_ * denominator_;
    result.denominator_ = denominator_ * other.denominator_;
    result.reduce();

    return result;
}

Rational Rational::operator-() const {
    Rational result = *this;
    result.numerator_ = -result.numerator_;

    return result;
}

Rational Rational::operator*(const Rational &other) const {
    Rational result;
    result.numerator_ = numerator_ * other.numerator_;
    result.denominator_ = denominator_ * other.denominator_;
    result.reduce();

    return result;
}

void Rational::reduce() {
    int64_t gcd = std::__gcd(std::abs(numerator_), std::abs(denominator_));

    numerator_ /= gcd;
    denominator_ /= gcd;

    if (denominator_ < 0) {
        numerator_ = -numerator_;
        denominator_ = -denominator_;
    }
}

Rational &Rational::operator+=(const Rational &other) {
    numerator_ *= other.denominator_;
    numerator_ += other.numerator_ * denominator_;
    denominator_ *= other.denominator_;

    reduce();

    return *this;
}

Rational &Rational::operator-=(const Rational &other) {
    numerator_ *= other.denominator_;
    numerator_ -= other.numerator_ * denominator_;
    denominator_ *= other.denominator_;

    reduce();

    return *this;
}

Rational &Rational::operator*=(const Rational &other) {
    numerator_ *= other.numerator_;
    denominator_ *= other.denominator_;

    reduce();

    return *this;
}

Rational &Rational::operator/=(const Rational &other) {
    if (other.numerator_ == 0) {
        throw std::runtime_error("cannot divide by zero");
    }

    numerator_ *= other.denominator_;
    denominator_ *= other.numerator_;

    reduce();

    return *this;
}

bool Rational::operator==(const Rational &other) const {
    return numerator_ == other.numerator_ && denominator_ == other.denominator_;
}

bool Rational::operator!=(const Rational &other) const {
    return numerator_ != other.numerator_ || denominator_ != other.denominator_;
}

std::ostream &operator<<(std::ostream &out, const Rational &rat) {
    out << rat.numerator_;
    if (rat.denominator_ != 1) {
        out << "/" << rat.denominator_;
    }
    return out;
}
