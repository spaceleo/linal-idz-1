//
// Created by spaceleo on 25.10.2021.
//

#ifndef UNTITLED1_RATIONAL_H
#define UNTITLED1_RATIONAL_H

#include <algorithm>
#include <cinttypes>
#include <cmath>
#include <iostream>

class Rational {
public:
    Rational();

    Rational(int64_t a);

    ~Rational() = default;

    Rational operator+(const Rational& other) const;

    Rational operator-() const;

    Rational operator*(const Rational &other) const;

    Rational &operator+=(const Rational &other);

    Rational &operator-=(const Rational &other);

    Rational &operator*=(const Rational &other);

    Rational &operator/=(const Rational &other);

    bool operator==(const Rational &other) const;

    bool operator!=(const Rational &other) const;

    friend std::ostream &operator<<(std::ostream &out, const Rational &rat);

private:
    int64_t numerator_;
    int64_t denominator_;

    void reduce();
};


#endif //UNTITLED1_RATIONAL_H
