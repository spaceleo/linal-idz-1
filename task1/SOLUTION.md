# Задача 1

Решаем методом Гаусса. Получаем:

![\Large x_1=\frac{1}{2}x_3-\frac{1}{2}x_4](https://latex.codecogs.com/svg.latex?\Large&space;x_1=\frac{1}{2}x_3-\frac{1}{2}x_4)

![\Large x_2=-x_3](https://latex.codecogs.com/svg.latex?\Large&space;x_2=-x_3)

Остальные переменные принимают любые значения.