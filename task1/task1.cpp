#include "../matrices/matrix.h"

#include <iostream>

int main() {
    Matrix m1 = Matrix::Array2D{
            {-2, 1,  -1},
            {4,  3,  7},
            {-3, 4,  1},
            {2,  -4, -2},
            {3,  -1, 2}
    };

    Matrix m2 = Matrix::Array2D{
            {3,  0, 0, 1},
            {1,  0, 1, 0},
            {-1, 1, 0, 0}
    };

    Matrix m3 = m1 * m2;

    std::cout << m3 << std::endl;

    auto solution = m3.getSolution();

    std::cout << solution;

    return 0;
}
