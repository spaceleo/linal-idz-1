#include "../matrices/matrix.h"
#include "../matrices/polymatrix.h"
#include "../numeric/rational.h"

#include <iostream>

int main() {
    Matrix a = Matrix::Array2D{
            {2, 2},
            {1, 1},
            {3, 3},
            {-2, -2},
            {-1, -1}
    };

    Matrix b = Matrix::Array2D{
            {3, -2, -1, 2, 1},
            {1, -3, 3, -1, 2}
    };

    Matrix c = a * b;
    PolyMatrix c_ = c;
    c_ = c_ - Polynomial({0, 1});
    std::cout << c_.determinant() << std::endl;

    return 0;
}
