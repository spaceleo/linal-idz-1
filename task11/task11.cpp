#include <iostream>
#include "../matrices/matrix.h"

int main() {
    Matrix m = Matrix::Array2D{
            {3, 3, -1, -7, -4, 2},
            {2, 2, 1,  2,  -1, 3},
            {3, 3, 2,  5,  -1, 5}
    };

    Row v1 = {0, 2, -7, 3, -3, -2};
    Row v2 = {3, 3, -6, 2, 1, -3};

    std::cout << m.checkSolution(v1) << "\n";
    std::cout << m.checkSolution(v2) << "\n\n";

    Matrix fss = m.getFundamentalSolutionSystem();
    std::cout << fss << "\n";

    Matrix tmp = Matrix::Array2D{
            {2, 3, -3, 2},
            {3, 2, 1, -3}
    };
    std::cout << tmp.gaussianElimination() << "\n";

    tmp = Matrix::Array2D{
            {2, 3, 0, 0},
            {3, 2, 0, 0},
            {-3, 1, 1, 0},
            {-2, -3, 0, 1}
    };
    std::cout << tmp.gaussianElimination() << "\n";

    std::cout << fss.transpose() * tmp << "\n";

    return 0;
}
