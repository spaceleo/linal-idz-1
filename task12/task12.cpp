#include <iostream>
#include "../matrices/matrix.h"

int main() {
    Matrix a = Matrix::Array2D{
            {2, -5, 2, -1},
            {4, -8, 2, -2},
            {4, -8, 2, -2},
            {-4, 7, -1, 2}
    };
        
    auto im = a.getImage();
    auto ker = a.getFundamentalSolutionSystem();

    std::cout << im << "\n" << ker << "\n";

    std::cout << a.gaussianElimination().concatenate(im.getFundamentalSolutionSystem()).getFundamentalSolutionSystem() << "\n";

    std::cout << im.concatenate(ker).gaussianElimination() << "\n";

    return 0;
}
