#include <iostream>
#include "../matrices/matrix.h"
#include "../matrices/polymatrix.h"

int main() {
    Matrix a = Matrix::Array2D{
            {-2, -1, -4, 2},
            {5, 2, 4, -3},
            {-1, -1, -5, 2},
            {0, -1, -9, 3}
    };

    PolyMatrix p(a);
    p = p - Polynomial({0, 1});
    std::cout << p.determinant() << "\n";

    return 0;
}
