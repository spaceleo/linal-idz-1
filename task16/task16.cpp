#include "../matrices/matrix.h"
#include "../matrices/polymatrix.h"
#include "../numeric/rational.h"

#include <iostream>

int main() {
    Matrix a = Matrix::Array2D{
            {-7, 3,  -5, 3},
            {-7, 2,  -7, 4},
            {3,  -2, 1,  -2},
            {3,  -2, 3,  -4}
    };
    Matrix b = Matrix::Array2D{
            {1,  5,  -1, -3},
            {-3, -8, 1,  4},
            {3,  4,  -3, -2},
            {-3, -6, 1,  2}
    };
    Matrix c = Matrix::Array2D{
            {1, 4, -2, -1},
            {-5, -8, 3, 2},
            {-7, -8, 2, 3},
            {3, 4, -2, -3}
    };

    PolyMatrix pa(a);
    pa = pa - Polynomial({0, 1});
    std::cout << pa.determinant() << std::endl;

    std::cout << (a + 2).gaussianElimination() << "\n";
    std::cout << ((a + 2) * (a + 2)).gaussianElimination() << "\n";

    PolyMatrix pb(b);
    pb = pb - Polynomial({0, 1});
    std::cout << pb.determinant() << std::endl;

    std::cout << (b + 2).gaussianElimination() << "\n";
    std::cout << ((b + 2) * (b + 2)).gaussianElimination() << "\n";

    PolyMatrix pc(c);
    pc = pc - Polynomial({0, 1});
    std::cout << pc.determinant() << std::endl;

    std::cout << (c + 2).gaussianElimination() << "\n";
    std::cout << ((c + 2) * (c + 2)).gaussianElimination() << "\n";

    return 0;
}
