#include "../matrices/matrix.h"
#include "../matrices/polymatrix.h"
#include "../numeric/rational.h"

#include <iostream>

int main() {
    Matrix a = Matrix::Array2D{
            {1,  5,  2,  0,  0,  0},
            {0,  7,  3,  0,  0,  0},
            {0,  -6, -2, 0,  0,  0},
            {-1, -2, -1, 3,  1,  0},
            {2,  -3, -2, -4, -1, 0},
            {1,  7,  4,  7,  2,  4}
    };

    PolyMatrix pa(a);
    pa = pa - Polynomial({0, 1});
    std::cout << pa.determinant() << std::endl;

    std::cout << (a - 4).gaussianElimination() << "\n";
    std::cout << (a - 1).gaussianElimination() << "\n";
    std::cout << ((a - 1) * (a - 1)).gaussianElimination() << "\n";

    return 0;
}
