#include "../matrices/matrix.h"
#include "../matrices/polymatrix.h"
#include "../numeric/rational.h"

#include <iostream>

int main() {
    Matrix a = Matrix::Array2D{
            {-2, 4,  -4},
            {1,  0,  -3},
            {2,  -2, -1}
    };
    Matrix b = Matrix::Array2D{
            {-5, 6, 9},
            {-1, 2, 2},
            {-2, 4, 4}
    };

    Matrix g = Matrix::Array2D{
            {1,  -1, -1},
            {1,  -2, 1},
            {-2, 3,  -1}
    };
    Matrix f = Matrix::Array2D{
            {1, -1, -2},
            {2, -1, -3},
            {-1, -1, 1}
    };

    a = g * a * g.inverse();
    b = f * b * f.inverse();

    std::cout << a * b << "\n";

    std::cout << a.getFundamentalSolutionSystem() << "\n";
    std::cout << b.getFundamentalSolutionSystem() << "\n";


    return 0;
}
