#include "../matrices/matrix.h"
#include "../matrices/polymatrix.h"
#include "../numeric/rational.h"

#include <iostream>

int main() {
    Matrix a = Matrix::Array2D{
            {0, 0, -3, -2},
            {0, 0, -4, -3},
            {-3, -4, 5, 7},
            {-2, -3, 7, 8}
    };
    Matrix v = Matrix::Array2D{
            {1},
            {-2},
            {1},
            {-1}
    };

    std::cout << (v.transpose() * a).getFundamentalSolutionSystem() << "\n";
    v = Matrix::Array2D{
            {1, -1},
            {-2, 1},
            {1, 0},
            {-1, 0}
    };

    std::cout << (v.transpose() * a).getFundamentalSolutionSystem() << "\n";
    v = Matrix::Array2D{
            {1, -1, 0},
            {-2, 1, 0},
            {1, 0, -1},
            {-1, 0, 1}
    };

    std::cout << (v.transpose() * a).getFundamentalSolutionSystem() << "\n";

    return 0;
}
