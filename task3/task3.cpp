#include "../matrices/matrix.h"

#include <iostream>

int main() {
    Row solution1 = {-9, -9, -7};
    Row solution2 = {-4, -3, 2};

    Matrix m = Matrix::Array2D{
            solution1,
            solution2
    };

    auto solution = m.getSolution();

    std::cout << solution << std::endl;

    Rational r1 = 13;
    r1 /= 3;
    Rational r2 = -46;
    r2 /= 9;

    for (int c = -10; c <= 10; ++c) {
        for (int f = -10; f <= 10; ++f) {
            Rational a = r1 * c;
            Rational b = r2 * c;
            Rational d = r1 * f;
            Rational e = r2 * f;

            Matrix kek = Matrix::Array2D{
                    {a, b, c},
                    {d, e, f}
            };

            if ((-4) * c == 2 * f && kek.checkSolution(solution1) && kek.checkSolution(solution2)) {
                std::cout << a << " " << b << " " << c << "\n" << d << " " << e << " " << f << "\n" << std::endl;
                std::cout << kek.getSolution({2, -4}) << std::endl;
                return 0;
            }
        }
    }

    return 0;
}
