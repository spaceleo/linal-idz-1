# Задача 4

Пусть надо найти все матрицы вида:

<pre>
    |x<sub>1</sub> x<sub>2</sub> x<sub>3</sub>|
B = |x<sub>4</sub> x<sub>5</sub> x<sub>6</sub>|
    |x<sub>7</sub> x<sub>8</sub> x<sub>9</sub>|
</pre>

Заметим, что матрицы <i>A</i> и <i>B</i> коммутируют тогда и только тогда, когда <i>AB - BA = 0</i>. Посчитаем <i>AB</i>
и <i>BA</i> и замечаем, что получилась однородная система линейных уравнений. Решаем ее методом Гаусса, получаем:

![x_1=x5](https://latex.codecogs.com/svg.latex?x_1=\frac{1}{2}x_3-\frac{1}{2}x_4)

![x_2=0](https://latex.codecogs.com/svg.latex?x_2=0)

![x_3=-3x_5+3x_9](https://latex.codecogs.com/svg.latex?x_3=-3x_5+3x_9)

![x_4=x_5-\frac{1}{3}x_6-x_9](https://latex.codecogs.com/svg.latex?x_4=x_5-\frac{1}{3}x_6-x_9)

![x_7=0](https://latex.codecogs.com/svg.latex?x_7=0)

![x_8=0](https://latex.codecogs.com/svg.latex?x_8=0)

Остальные переменные могут принимать любые значения.