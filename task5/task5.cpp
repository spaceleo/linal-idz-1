#include "../matrices/matrix.h"

#include <iostream>

int main() {
    Matrix m = Matrix::Array2D{
            {-1, 0,  0,  2,  0,  0},
            {4,  0,  0,  0,  2,  0},
            {2,  0,  0,  0,  0,  2},
            {0,  -1, 0,  1,  0,  0},
            {0,  4,  0,  0,  1,  0},
            {0,  2,  0,  0,  0,  1},
            {0,  0,  -1, -1, 0,  0},
            {0,  0,  4,  0,  -1, 0},
            {0,  0,  2,  0,  0,  -1}
    };
    std::vector<Rational> result = {3, -2, -4, -1, 9, 3, -2, 3, 3};

    auto solution = m.getSolution(result);

    std::cout << solution << std::endl;

    // a1=-1 a2=2 a3=1 b1=1 b2=1 b3=-1

    Matrix m1 = Matrix::Array2D{
            {2, -1},
            {1, 2},
            {-1, 1}
    };

    Matrix m2 = Matrix::Array2D{
            {1, 1, -1},
            {-1, 4, 2}
    };

    std::cout << m1 * m2 << std::endl;

    std::cout << m2 * m1 << std::endl;

    return 0;
}
