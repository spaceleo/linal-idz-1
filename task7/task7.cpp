#include "../matrices/matrix.h"

#include <iostream>

int main() {
    Matrix a = Matrix::Array2D{
            {3,  -2, -2, 1},
            {-2, 2,  2,  -3},
            {-3, -1, 3,  -1},
            {-1, 2,  1,  2}
    };
    Matrix b = Matrix::Array2D{
            {3,  -2, 1,  2},
            {-1, 3,  1,  -2},
            {-3, 3,  1,  -3},
            {2,  -2, -2, -1}
    };
    Matrix c = Matrix::Array2D{
            {1,  -1, 1,  1},
            {-1, 2,  -2, -2},
            {1,  -2, 3,  3},
            {1,  -2, 1,  2}
    };
    Matrix d = Matrix::Array2D{
            {-4, -2, -3, -9},
            {-5, -6, -2, -1},
            {-4, 5,  -2, 6},
            {7,  -3, 3,  -1}
    };

    Matrix x = (a.inverse() * c * b.inverse()).inverse() - d;
    std::cout << x << std::endl;

    std::cout << (a * (x + d).inverse() * b) << std::endl;

    return 0;
}
