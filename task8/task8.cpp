#include "../matrices/matrix.h"
#include "../matrices/polymatrix.h"
#include "../numeric/rational.h"

#include <iostream>

int main() {
    Matrix a = Matrix::Array2D{
            {1, -2, -1, -5},
            {1, -1, 5, 1},
            {-1, 1, -5, 2},
            {0, 0, -1, -2}
    };

    Matrix a_ = (a * a - 4).inverse();
    std::cout << (a_ * a_).determinant() << std::endl;

    PolyMatrix p(a);
    p = p - Polynomial({0, 1});
    std::cout << p.determinant() << std::endl;

    return 0;
}
