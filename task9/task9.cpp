#include "../matrices/matrix.h"
#include "../matrices/polymatrix.h"
#include "../numeric/rational.h"

#include <iostream>

int main() {
    PolyMatrix a = PolyMatrix::Array2D({{{-6},   {9},    {3},    {-6},   {0, 1}, {8},    {0, 1}},
                                        {{1},    {-7},   {9},    {-8},   {3},    {0, 1}, {2}},
                                        {{8},    {7},    {0, 1}, {-8},   {-6},   {6},    {1}},
                                        {{4},    {3},    {-5},   {0, 1}, {-4},   {3},    {4}},
                                        {{5},    {0, 1}, {8},    {-9},   {-8},   {-6},   {-5}},
                                        {{0, 1}, {-5},   {-4},   {4},    {7},    {2},    {8}},
                                        {{9},    {-2},   {-6},   {0, 1}, {8},    {-1},   {-9}}});

    std::cout << a.determinant() << std::endl;

    return 0;
}
